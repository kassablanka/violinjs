import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/bootswatch.min.css';
import App from './components/App/indes';

ReactDOM.render(<App />, document.getElementById('root'));
