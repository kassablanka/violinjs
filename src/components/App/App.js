import React, { Component } from 'react';
import { Col, Container, ProgressBar, Row } from 'react-bootstrap';
import Header from '../Header';
import SearchPanel from "../SearchPanel";
import ItemStatusFilter from '../ItemStatusFilter';
import TodoList from '../TodoList';
import ItemAddForm from '../ItemAddForm';
import './App.css'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: [
                this.createTodoItem('Drink coffee'),
                this.createTodoItem('Make Awesome App'),
                this.createTodoItem('Have a lunch')
            ],
            term: '',
            filter: 'all'
        };

        this.deleteItem = this.deleteItem.bind(this);
    }

    maxTaskId = 100;

    createTodoItem = (taskName) => {
        return {
            taskName,
            variant: '',
            taskId: this.maxTaskId++
        }
    };

    toggleProperty(arr, taskId, propName) {
        let index = arr.findIndex((el) => el.taskId === taskId);

        let oldItem = arr[index];
        let newItem;

        if (oldItem.variant === '' || oldItem.variant !== propName) {
            newItem = { ...oldItem, variant: propName }
        } else {
            newItem = { ...oldItem, variant: '' }
        }

        return [
            ...arr.slice(0, index),
            newItem,
            ...arr.slice(index + 1)
        ]
    }

    onToggleImportant = (taskId) => {
        this.setState(({ item }) => {
            return {
                item: this.toggleProperty(item, taskId, 'warning')
            }
        });
    };

    onToggleDone = (taskId) => {
        this.setState(({ item }) => {
            return {
                item: this.toggleProperty(item, taskId, 'success')
            }
        });
    };

    deleteItem = (taskId) => {
        this.setState(({ item }) => {
            let index = item.findIndex((el) => el.taskId === taskId);

            let newArray = [
                ...item.slice(0, index),
                ...item.slice(index + 1)
            ];

            return {
                item: newArray
            }
        });
    };

    addItem = (taskName) => {
        let newItem = this.createTodoItem(taskName);

        this.setState(({ item }) => {
            let newArray = [
                ...item,
                newItem
            ];

            return {
                item: newArray
            }
        });
    };

    onSearchChange = (term) => {
        this.setState({ term });
    };

    search = (items, term) => {
        if (term.length === 0) {
            return items;
        }

        return items.filter((item) => {
            return item.taskName
                .toLowerCase()
                .indexOf(term.toLowerCase()) > -1;
        });
    };

    filter = (items, filter) => {
        switch (filter) {
            case 'all':
                return items;
            case 'active':
                return items.filter((item) => item.variant === '');
            case 'done':
                return items.filter((item) => item.variant === 'success');
            default:
                return items;
        }
    };

    onFilterChange = (filter) => {
        this.setState({ filter });
    };

    render() {
        const { item, term, filter } = this.state;
        const visibleItems = this.filter(this.search(item, term), filter);
        const importantCount = item.filter((el) => el.variant === 'warning').length;
        const activeCount = item.filter((el) => el.variant === '').length;
        const doneCount = item.filter((el) => el.variant === 'success').length;
        const maxProgressBar = visibleItems.length;
        const todoCount = item.length - doneCount;

        return (
            <Container>
                <Header doneCount={ doneCount } todoCount={ todoCount }/>
                <Row className='justify-content-md-center'>
                    <Col xs lg='6'>
                        <ProgressBar>
                            <ProgressBar animated variant='success' now={ doneCount } max={ maxProgressBar } />
                            <ProgressBar animated variant='warning' now={ importantCount } max={ maxProgressBar } />
                            <ProgressBar animated variant='info' now={ activeCount } max={ maxProgressBar } />
                        </ProgressBar>
                    </Col>
                </Row>
                <Row className='justify-content-md-center'>
                    <Col xs lg='6'>
                        <div className="search-todo-list">
                            <SearchPanel onSearchChange={ this.onSearchChange }/>
                            <ItemStatusFilter filter={ filter } onFilterChange={ this.onFilterChange }/>
                        </div>
                    </Col>
                </Row>
                <TodoList todoList={ visibleItems } onDeleted={ this.deleteItem }
                          onToggleImportant={ this.onToggleImportant }
                          onToggleDone={ this.onToggleDone }/>
                <ItemAddForm addItem={ this.addItem }/>
            </Container>
        )
    }
}
export default App;
