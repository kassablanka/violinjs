import React, { Component } from 'react';
import { Button, FormGroup, ListGroup } from 'react-bootstrap';

class TodoListItem extends Component {
    render() {
        const { todoListItem, onDeleted, onToggleImportant, onToggleDone } = this.props;

        return (
            <ListGroup.Item variant={ todoListItem.variant }>
                <span>{ todoListItem.taskName }</span>
                <FormGroup>
                    <Button variant='success' onClick={ onToggleDone }>
                        <i className='fas fa-check'/>
                    </Button>
                    <Button variant='warning' onClick={ onToggleImportant }>
                        <i className='fas fa-exclamation'/>
                    </Button>
                    <Button variant='secondary' onClick={ onDeleted }>
                        <i className='fas fa-trash-alt'/>
                    </Button>
                </FormGroup>
            </ListGroup.Item>
        );
    }
}

export default TodoListItem;
