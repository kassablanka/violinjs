import React, { Component } from 'react';
import { Button, Col, Form, FormControl, InputGroup, Row } from 'react-bootstrap';
import './ItemAddForm.css';

class NameClass extends Component {
    constructor(props) {
        super(props);

        this.state = {
            taskName: '',
            termLength: true
        }
    }

    onLabelChange = (e) => {
        this.setState({
            taskName: e.target.value
        });

        if (e.target.value.trim() === '') {
            this.setState({
                termLength: true
            });
        } else if (e.target.value.length <= 6) {
            this.setState({
                termLength: true
            });
        } else {
            this.setState({
                termLength: false
            });
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addItem(this.state.taskName);

        this.setState({
            taskName: ''
        });
    };

    render() {
        const { taskName, termLength } = this.state;

        return (
            <Row className='justify-content-md-center'>
                <Col xs lg='6'>
                    <Form onSubmit={ this.onSubmit }>
                        <InputGroup className='mb-3'>
                            <FormControl placeholder='What needs to be done' value={ taskName }
                                         onChange={ this.onLabelChange }/>
                            <InputGroup.Append>
                                <Button type="submit" variant='info'
                                        disabled={ termLength ? true : '' }>Добавить</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form>
                </Col>
            </Row>
        );
    }
}

export default NameClass;
