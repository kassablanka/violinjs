import React, { Component } from 'react';
import { Alert, Col, ListGroup, Row } from 'react-bootstrap';
import TodoListItem from "../TodoListItem/TodoListItem";
import './TodoList.css';

class TodoList extends Component {
    render() {
        const { todoList, onDeleted, onToggleImportant, onToggleDone } = this.props;

        return (
            <Row className='justify-content-md-center'>
                <Col xs lg='6'>
                    { todoList.length === 0 ?
                        <Alert variant='light'>Task list is empty</Alert> :
                        <ListGroup>
                            { todoList.map((item) =>
                                <TodoListItem key={ item.taskId } todoListItem={ item }
                                              onDeleted={ () => onDeleted(item.taskId) }
                                              onToggleImportant={ () => onToggleImportant(item.taskId) }
                                              onToggleDone={ () => onToggleDone(item.taskId) }/>
                            )}
                        </ListGroup>
                    }
                </Col>
            </Row>
        );
    }
}

export default TodoList;
