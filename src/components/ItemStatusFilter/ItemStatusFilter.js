import React, { Component } from 'react';
import { Button, ButtonGroup } from "react-bootstrap";

class ItemStatusFilter extends Component {

    buttons = [
        { name: 'all', label: 'All' },
        { name: 'active', label: 'Active' },
        { name: 'done', label: 'Done' }
    ];

    render() {
        const { filter, onFilterChange } = this.props;

        const buttons = this.buttons.map(({ name, label }) => {
            const isActive = filter === name;
            const variant = isActive ? 'info' : 'outline-info';

            return (
                <Button variant={ variant } onClick={ () => onFilterChange(name) } key={ name }>{ label }</Button>
            );
        });

        return (
            <ButtonGroup aria-label='Basic example'>
                { buttons }
            </ButtonGroup>
        );
    }
}

export default ItemStatusFilter;
