import React, { Component } from 'react';
import { Col, Container, Jumbotron, Row } from "react-bootstrap";
import './Header.css'

class Header extends Component {
    render() {
        const { doneCount, todoCount } =this.props;

        return (
            <Row className='justify-content-md-center'>
                <Col xs lg='6'>
                    <Jumbotron>
                        <Container>
                            <h1>To do list</h1>
                            <p>{ todoCount } more to do, { doneCount } done</p>
                        </Container>
                    </Jumbotron>
                </Col>
            </Row>
        )
    }
}

export default Header;
