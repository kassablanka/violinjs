import React, { Component } from 'react';
import { FormControl, InputGroup } from "react-bootstrap";

class SearchPanel extends Component {
    state = {
        term: ''
    };

    onSearchChange = (e) => {
        const term = e.target.value;
        this.setState({ term });
        this.props.onSearchChange(term);
    };

    render() {
        return (
            <InputGroup className='mb-3'>
                <FormControl placeholder='Type to search' value={ this.state.term } onChange={ this.onSearchChange }/>
            </InputGroup>
        );
    }
}

export default SearchPanel;
